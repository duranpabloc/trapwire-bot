import { Module, Logger, HttpModule } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ConfigModule } from './config/config.module';
import { TournamentsModule } from './modules/tournaments/tournaments.module';
import { TeamsModule } from './modules/teams/teams.module';
import { SsoModule } from './modules/sso/sso.module';
import { AuthModule } from './modules/auth/auth.module';
import { ProfileModule } from './modules/profile/profile.module';
import { IntegrationsModule } from './modules/integrations/integrations.module';
import { ApiriotModule } from './modules/apis/apiriot/apiriot.module';
import { ApiRiotService } from './modules/apis/apiriot/apiriot.service';

@Module({
  imports: [
    ConfigModule,
    ApiriotModule,
    AuthModule,
    HttpModule.register({
      timeout: 5000,
      maxRedirects: 5,
    }),
    MongooseModule.forRoot('mongodb+srv://twdevops:Lcu08aYPHNftbTyZ@cluster0.lf6bz.gcp.mongodb.net/trapwire-db?retryWrites=true&w=majority'),
    TournamentsModule,
    TeamsModule,
    SsoModule,
    IntegrationsModule,
    ProfileModule
  ],
  controllers: [AppController],
  providers: [AppService, Logger, ApiRiotService],
})
export class AppModule {}
