import { Injectable, Logger } from '@nestjs/common';
import { Client, Message } from 'discord.js';
import { ApiRiotService } from './modules/apis/apiriot/apiriot.service';
import { Config } from './config/config';

@Injectable()
export class AppService {

  private bot: Client = new Client();
  private readonly logger = new Logger(AppService.name);

  constructor(private config: Config, private readonly apiRiot: ApiRiotService) {
    this.bot.login(this.config.discordToken);
    this.bot.on('ready', () => {
      this.logger.log('Bot is ready!');
    });

    this.bot.on('message', async (message: Message) => {
      if (message.content.startsWith(`${this.config.prefix}`)) {
        switch (message.content.split(" ")[1]) {
          case 'hola': {
            this.hola(message);
            break;
          }
          case 'dile': {
            this.dile(message);
            break;
          }
          case 'bindtag': {
            /* this.bindTag(message); */
            break;
          }
          case 'unbindtag': {
            this.unbindTag(message);
            break;
          }
          case 'users': {
            /* this.users(message); */
            break;
          }
          case 'delmsg': {
            const command = message.content.split(" ")[2];

            if (command) {
              this.delmsg(message, command);
            }

            break;
          }
          case 'status': {
            this.status(message);
            break;
          }
          case 'author': {
            this.author(message);
            break;
          }
          case 'commands': {
            this.commands(message);
            break;
          }
          case 'register':{
            /* this.register(message); */
            break;
          }
          case 'ping': {
            message.channel.send(`pong`);
            break;
          }
          case 'iniciativa': {
            const dado = Math.round(1 + (Math.random() * 19));
            await message.channel.send(`${message.author.username} - Dado por iniciativa: ${dado}`).then(msg => {
              msg.delete({timeout: 3000});
              message.delete({timeout: 3000});
            });
            break;
          }
          case 'meimporta': {
            const dado = Math.round(1 + (Math.random() * 100000000));
            await message.channel.send(`${message.author.username}: Me importa ${dado} hectáreas de corneta`).then(msg => {
              msg.delete({ timeout: 3000 });
              message.delete({ timeout: 3000 });
            });
            break;
          }
          case 'importancia': {
            const dado = Math.round(1 + (Math.random() * 19));
            await message.channel.send(`${message.author.username} - Déjame rolear cuánto me importa: ${dado}`).then(() => {
              message.delete();
            });
            break;
          }
          default: {
            message.channel.send(`Ese comando no existe`).then(msg => {
              msg.delete({ timeout: 3000 });
              message.delete({ timeout: 3000 });
            });
            break;
          }
        }
      }
    });
  }

 /*  async register(message: Message){
    let user:ProfilesDto = new ProfilesDto();
    if(user = await this.usersService.getUserByDiscordId(message.author.id)){
      await message.channel.send(`El usuario <@${user.discordId}> está registrado como ${user.gameName}#${user.tagLine}`).then(msg => {
        msg.delete({ timeout: 3000 });
        message.delete({ timeout: 3000 });
      });
    }
  } */

  async hola(message: Message) {
    this.logger.debug(message.mentions.members.first());
    message.channel.send(`Weeeeena ${message.author.username}, demos gracias a diosito que vienes con tu carita tan bonita por acá UwU`).then(msg => {
      msg.delete({ timeout: 3000 })
      message.delete({ timeout: 3000 });
    });
  }

  async dile(message: Message) {
    this.logger.debug(message.mentions.members.first());
    message.channel.send(`${message.mentions.members.first()}, sos un ghei, se sabe`).then(msg => {
      msg.delete({ timeout: 3000 });
    });
  }

  /* async bindTag(message: Message) {
    try {
      message.channel.send(await this.processTag(message)).then(msg => {
        msg.delete({ timeout: 3000 });
        message.delete({ timeout: 3000 });
      });
    } catch (error) {
      message.channel.send(error.message).then(msg => {
        msg.delete({ timeout: 3000 });
        message.delete({ timeout: 3000 });
      });
    }
  } */

  async unbindTag(message: Message) {
    try {
      this.deleteTag(message);
    } catch (error) {
      message.channel.send(error.message).then(msg => {
        msg.delete({ timeout: 5000 });
      });
    }
  }

  /* async users(message: Message) {
    message.channel.send({
      embed: {
        color: 0,
        description: await this.listUsers(await this.usersService.getAllUsers())
      }
    });
  } */

  async delmsg(message: Message, command: string) {
    const num = Number.parseInt(command.replace(this.config.prefix, ''));
    if (!isNaN(num)) {
      this.deleteMsg(message, num);
    }
  }


  async deleteMsg(message: Message, num: number) {
    const messages = await message.channel.fetch();
    await message.channel.bulkDelete(num);
  }

  author(message: Message) {
    message.channel.send(`This bot was developed by <@228244975345860609> for VALORANT Chile's community`).then(msg => {
      msg.delete({ timeout: 5000 });
      message.delete({ timeout: 5000 });
    });
  }

  async commands(message: Message) {
    message.channel.send({
      embed: {
        color: 100,
        description: 'Comandos de TrapWire-bot: \n\n1.- !tw bindtag [Riot#TAG] ---> Asocia un tag a su cuenta\n2.- !tw users ---> Listado de usuarios registrados\n3.- !tw status ---> Estado del bot\n4.- !tw author ---> Info del autor\n'
      }
    });
  }

  async status(message: Message) {
    message.channel.send(`This bot is in an Alpha state, waiting for the VALORANT API to get data`).then(msg => {
      msg.delete({ timeout: 3000 });
      message.delete({ timeout: 3000 });
    });
  }

  async deleteTag(message: Message) {
    /* try {
      this.logger.debug(message.author.id);
      if (await this.usersService.getUserByDiscordId(message.author.id)) {
        await this.usersService.deleteUserByDiscordId(message.author.id);
        message.channel.send('Usuario borrado').then(msg => {
          msg.delete({ timeout: 3000 });
        });
      } else {
        throw new Error('No estás registrado');
      }
    } catch (error) {
      this.logger.error(error);
      throw new Error(`No se pudo eliminar: ${error.message}`);
    } */

  }
  /* async processTag(message: Message): Promise<string> {
    if (!await this.usersService.getUserByDiscordId(message.author.id)) {
      let user: ProfilesDto = {} as ProfilesDto;
      user.discordId = message.author.id;
      user = await this.addTag(user, message.content);
      try {
        const response = await this.apiRiot.getPuuid(user);
        this.logger.debug('getPuuid', response);
        try {
          user.puuid = response.puuid;
          this.logger.debug(user);
          await this.usersService.createUser(user);
          return 'Se realizó el registro con éxito';
        } catch (error) {
          this.logger.error(error);
          throw error;
        }

      } catch (error) {
        this.logger.error(error);
        throw error;
      }

    } else {
      this.logger.error(`${message.author.username} ya tiene registrado un tag`);
      throw new Error(`${message.author.username} ya tiene registrado un tag`);
    }
  } */

  /* async addTag(user: ProfilesDto, message: string): Promise<ProfilesDto> {
    try {
      const userSplit = message.split(" ")[2].replace(this.config.prefix, '').split('#');
      this.logger.log(userSplit);
      user.gameName = userSplit[0];
      user.tagLine = userSplit[1].toUpperCase();
      return user;
    } catch (error) {
      this.logger.error(error);
      throw new Error('Tag sin formato válido')
    }
  } */

  /* async listUsers(users: Profiles[]) {
    let list: string = "";
    let cont: number = 0;
    users.map((user: Profiles) => {
      cont++;
      list += `${cont}.- ${user}\n`;
    });
    this.logger.log(`Se listaron ${cont} jugadores`)
    return list;
  } */

  async assignRoleToUser(discordId: string, role: string, message: Message) {
    let rMember = message.guild.member(message.mentions.users.first());
    if (!rMember) return message.reply('No se pudo obtener ese usuario');
  }
}
