import { Module } from '@nestjs/common';
import { Config } from './config';

const config = {
  provide: Config,
  useValue: new Config(`${process.env.NODE_ENV}.env`),
};

@Module({
    providers: [config],
    exports: [config],
})
export class ConfigModule {}
