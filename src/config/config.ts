import { Injectable } from '@nestjs/common';
import * as fs from 'fs';
import * as dotenv from 'dotenv';

export interface EnvConfig {
  [key: string]: string;
}

@Injectable()
export class Config {
  private envConfig: EnvConfig;

  constructor(filePath: string) {
    try{
    const config = dotenv.parse(fs.readFileSync(filePath));
    this.envConfig = config;
    }catch{
      this.envConfig = {};
    }
  }

  get discordToken(): string {
    const discordToken = process.env.DISCORD_TOKEN || this.envConfig.DISCORD_TOKEN;
    return discordToken;
  }

  get prefix(): string {
    const prefix = process.env.PREFIX || this.envConfig.PREFIX;
    return prefix;
  }

  get apiRiotBaseUrl():string {
    const apiRiotBaseUrl = process.env.API_RIOT_BASE_URL || this.envConfig.API_RIOT_BASE_URL;
    return apiRiotBaseUrl;
  }

  get apiRiotToken(): string{
    const apiRiotToken = process.env.API_RIOT_TOKEN || this.envConfig.API_RIOT_TOKEN;
    return apiRiotToken;
  }
  get dbUser(): string{
    const dbUser = process.env.DB_USER || this.envConfig.DB_USER;
    return dbUser;
  }
  get dbPassword(): string{
    const dbUser = process.env.DB_PASSWORD || this.envConfig.DB_PASSWORD;
    return dbUser;
  }
  get dbUri(): string{
    const dbUri = process.env.DB_URI || this.envConfig.DB_URI;
    return dbUri;
  }
  get dbName(): string{
    const dbName = process.env.DB_NAME || this.envConfig.DB_NAME;
    return dbName;
  }

  get isSecurityActive(): boolean{
    const securityActive = process.env.SECURITY_ACTIVE || this.envConfig.SECURITY_ACTIVE;
    return Boolean(securityActive==='true'?true:false);
  } 

  get Secret(): string{
    const secret = process.env.SECRET || this.envConfig.SECRET;
    return secret;
  }

  get expiresIn(): string{
    const expiresIn = process.env.EXPIRES_IN || this.envConfig.EXPIRES_IN;
    return expiresIn;
  }
}
