import { Module, HttpModule } from '@nestjs/common';
import { ApiRiotService } from './apiriot.service';
import { ConfigModule } from 'src/config/config.module';

@Module({
  imports: [HttpModule, ConfigModule],
  providers: [ApiRiotService],
  exports: [ApiRiotService]
})
export class ApiriotModule {}
