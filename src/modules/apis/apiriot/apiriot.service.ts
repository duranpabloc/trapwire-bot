import { Injectable, Logger, HttpService } from '@nestjs/common';
import { Config } from 'src/config/config';

@Injectable()
export class ApiRiotService {
    private readonly logger = new Logger(ApiRiotService.name);

    private headers = {};

    constructor(
        private httpService: HttpService,
        private config: Config) {
        this.headers = { headers: { 'X-Riot-Token': this.config.apiRiotToken } };
    }

    async getPuuidByTag(tag: string): Promise<any> {
        try {
            const gameName = tag.split('-')[0];
            const tagLine = tag.split('-')[1];
            const response = await this.httpService.get(`${this.config.apiRiotBaseUrl}/riot/account/v1/accounts/by-riot-id/${gameName}/${tagLine}`, this.headers).toPromise();
            return response.data;
        } catch (error) {
            this.logger.error(error);
            throw error.message;
        }
    }
}
