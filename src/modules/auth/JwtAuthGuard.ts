import {
  ExecutionContext,
  Injectable,
} from '@nestjs/common';
import { verify } from 'jsonwebtoken';
import { AuthGuard } from '@nestjs/passport';

@Injectable()
export class JwtAuthGuard extends AuthGuard('jwt') {

  constructor() {
    super();
  }

  canActivate(context: ExecutionContext) {
    try {
      const isSecurityActive = false;
      if (!isSecurityActive) {
        return true;
      }
      else {
        const token = context.switchToHttp().getRequest().headers['authorization'].split(' ')[1];
        verify(token, 'secretKey');
        return true;
      }
    } catch
    {
      return false;
    }
  }

}
