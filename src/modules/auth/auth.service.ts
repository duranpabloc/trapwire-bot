import { Injectable } from '@nestjs/common';
import { sign, decode } from 'jsonwebtoken';
import { SsoService } from 'src/modules/sso/sso.service';
import { SsoUserDto } from 'src/modules/sso/models';

@Injectable()
export class AuthService {


  constructor(
    private ssoService: SsoService
  ) { }

  async validateUser(ssoUserDto: SsoUserDto): Promise<boolean> {
    const user = await this.ssoService.getUserByUserPassword(ssoUserDto);
    if (!user) {
      return false;
    }
    return true;
  }

  async signPayload(payload: any) {
    return sign(payload._doc, 'secretKey', { expiresIn: '1d' })
  }

  decodeJwt(jwt: string): any{
    return decode(jwt, {json: true});
  }
}