import { ExtractJwt, Strategy, VerifiedCallback } from 'passport-jwt';
import { PassportStrategy } from '@nestjs/passport';
import { HttpException, HttpStatus, Injectable, Logger } from '@nestjs/common';
import { AuthService } from './auth.service';
import { Config } from 'src/config/config';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {

  private readonly logger = new Logger(JwtStrategy.name);
  
  constructor(private authService: AuthService) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      ignoreExpiration: false,
      secretOrKey: 'secretKey',
    });
  }

  async validate(payload: any, done: VerifiedCallback) {
    const { email } = payload;
    const userValidated = await this.authService.validateUser(email);

    if(!userValidated){
      return done(
        new HttpException('Unauthorized Access', HttpStatus.UNAUTHORIZED), 
        false
      );
    }

    return done(null, userValidated ,payload.iat);
  }
}