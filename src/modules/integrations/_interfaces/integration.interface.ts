import { Profile, ProfileDto } from "src/modules/profile/models/profile.model";

export interface IntegrationService{

    getProfileByAccountName(accountName: string): Promise<Profile>;

    createProfile(profile: ProfileDto): Promise<Profile>;

    modifyProfile(profile: ProfileDto): Promise<Profile>;

    deleteProfile(accountName: string);
}