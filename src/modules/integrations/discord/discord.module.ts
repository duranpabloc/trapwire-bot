import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { ProfileSchema } from 'src/modules/profile/models';
import { ProfileModule } from 'src/modules/profile/profile.module';
import { DiscordService } from './discord.service';

@Module({
  imports: [MongooseModule.forFeature([{name: 'Profile', schema: ProfileSchema}]), ProfileModule],
  controllers: [],
  providers: [DiscordService],
  exports: [DiscordService]
})
export class DiscordModule {}
