import { Injectable, Logger } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { PlatformName, Profile, ProfileDto } from 'src/modules/profile/models';
import { IntegrationService } from '../_interfaces';

@Injectable()
export class DiscordService implements IntegrationService {

    constructor(@InjectModel('Profile') private readonly profileModel: Model<Profile>) { }

    

    private readonly logger = new Logger(DiscordService.name);

    async getProfileByAccountName(discordId: string): Promise<Profile> {
        try {
            const profile = await this.profileModel.findOne({ accountName: discordId, platformName: PlatformName.DISCORD });
            return profile as Profile;
        } catch (error) {
            this.logger.error(error);
        }
    }

    createProfile(profile: ProfileDto): Promise<Profile> {
        throw new Error('Method not implemented.');
    }
    modifyProfile(profile: ProfileDto): Promise<Profile> {
        throw new Error('Method not implemented.');
    }

    async deleteProfile(accountName: string) {
        try {
            const result = await this.profileModel.deleteOne({ accountName: accountName, platformName: PlatformName.DISCORD }).exec();
        } catch (error) {
            this.logger.error(error);
        }
    }
}
