import { Controller, Get, HttpCode, Logger, Param, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiOperation, ApiParam, ApiTags } from '@nestjs/swagger';
import { Profile, ProfileDto } from 'src/modules/profile/models/profile.model';
import { JwtAuthGuard } from '../auth/JwtAuthGuard';
import { Roles } from '../role/decorators/role.decorator';
import { RoleGuard } from '../role/guards/role.guard';
import { UserStatus } from '../sso/models';
import { Status } from '../status/decorators/status.decorator';
import { StatusGuard } from '../status/guards/status.guard';
import { DiscordService } from './discord/discord.service';
import { RiotService } from './riot/riot.service';

@ApiTags('integrations')
@ApiBearerAuth()
@Controller('integrations')
export class IntegrationsController {
    /* STORAGE */

    private readonly logger = new Logger(IntegrationsController.name);

    constructor(
        private readonly discordService: DiscordService,
        private readonly riotService: RiotService
    ) { }

    @Get('apiriot/tag/:riotTag')
    @HttpCode(200)
    @ApiParam({ name: 'riotTag', description: 'tag de Riot', required: true, example: 'KrakeX-LAS' })
    @ApiOperation({ summary: 'Es endpoint de prueba' })
    @Roles('admin', 'user')
    @Status(UserStatus.VALIDATED)
    @UseGuards(JwtAuthGuard, RoleGuard, StatusGuard)
    async getUuidByRiotTag(@Param('riotTag') tag: string): Promise<Profile> {
        try {
            let profile: ProfileDto;
            profile.accountName
            return await this.riotService.getPuuidByTag(tag);
        }
        catch (error) {
            this.logger.error(error);
        }
    }

    @Get('discord/:accountName')
    @HttpCode(200)
    @ApiParam({ name: 'accountName', description: 'id discord', required: true, example: '228244975345860609' })
    @ApiOperation({ summary: 'Obtiene un usuario por cuenta de discord' })
    @Roles('admin', 'user')
    @Status(UserStatus.VALIDATED)
    @UseGuards(JwtAuthGuard, RoleGuard, StatusGuard)
    async getGlobalUserByDiscordProfile(@Param('accountName') accountName: string): Promise<Profile> {
        try {
            const user = await this.discordService.getProfileByAccountName(accountName);
            return user;
        }
        catch (error) {
            this.logger.error(error);
        }
    }
}
