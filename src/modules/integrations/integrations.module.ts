import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { ApiriotModule } from 'src/modules/apis/apiriot/apiriot.module';
import { ProfileSchema } from 'src/modules/profile/models';
import { AuthModule } from '../auth/auth.module';
import { RoleGuard } from '../role/guards/role.guard';
import { DiscordModule } from './discord/discord.module';
import { DiscordService } from './discord/discord.service';
import { IntegrationsController } from './integrations.controller';
import { IntegrationsService } from './integrations.service';
import { RiotModule } from './riot/riot.module';
import { RiotService } from './riot/riot.service';

@Module({
  imports: [
    MongooseModule.forFeature([{name: 'Profile', schema: ProfileSchema}]),
    DiscordModule,
    ApiriotModule,
    RiotModule,
    AuthModule
  ],
  controllers: [IntegrationsController],
  providers: [IntegrationsService, DiscordService, RiotService]
})
export class IntegrationsModule {}
