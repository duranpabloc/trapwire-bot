import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { ApiriotModule } from 'src/modules/apis/apiriot/apiriot.module';
import { ProfileSchema } from 'src/modules/profile/models';
import { RiotService } from './riot.service';

@Module({
  imports: [MongooseModule.forFeature([{name: 'Profile', schema: ProfileSchema}]), ApiriotModule],
  controllers: [],
  providers: [RiotService],
  exports: [RiotService]
})
export class RiotModule {}
