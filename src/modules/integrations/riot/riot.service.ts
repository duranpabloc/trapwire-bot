import { Injectable, Logger } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { ApiRiotService } from 'src/modules/apis/apiriot/apiriot.service';
import { PlatformName, Profile, ProfileDto } from 'src/modules/profile/models/profile.model';
import { IntegrationService } from '../_interfaces';

@Injectable()
export class RiotService implements IntegrationService{

    private readonly logger = new Logger(RiotService.name);

    constructor(@InjectModel('Profile') private readonly profileModel: Model<Profile>, private readonly apiRiotService: ApiRiotService) { }

    async getProfileByAccountName(accountName: string): Promise<Profile> {
        const user = await this.profileModel.findOne({ accountName: accountName, platformName: PlatformName.RIOT_GAMES });
        return user as Profile;
    }
    modifyProfile(profile: ProfileDto): Promise<Profile> {
        throw new Error('Method not implemented.');
    }

    async createProfile(profile: ProfileDto): Promise<Profile> {
        if (!(await this.getProfileByAccountName(profile.accountName))) {
            try {
                const newProfile = new this.profileModel({ ...profile });
                const result = await newProfile.save();
                return result;
            } catch (error) {
                this.logger.error(error);
                throw new Error(`Problema al guardar: ${error.message}`);
            }
        } else {
            throw new Error('Ese tag ya está registrado por otro usuario');
        }

    }

    async deleteProfile(accountName: string){
        try {
            const result = await this.profileModel.deleteOne({ accountName: accountName, platformName: PlatformName.DISCORD }).exec();
        } catch (error) {
            this.logger.error(error);
        }
    }

    async getPuuidByTag(tag: string): Promise<any> {
        try {
            return await this.apiRiotService.getPuuidByTag(tag);
        } catch (error) {
            this.logger.error(error);
            throw error.message;
        }
    }
}
