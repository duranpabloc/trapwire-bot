import { Schema } from 'mongoose';
import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import * as mongoose from 'mongoose';
import { GlobalUser } from 'src/modules/sso/models';
import { Team, TeamDto } from 'src/modules/teams/team.model';

export const ProfileSchema = new mongoose.Schema({
    ownerId: { type: mongoose.Types.ObjectId, ref: 'GlobalUser', required: true},
    platformName: { type: String, required: true },
    accountName: { type: String, required: true },
    emailRegistered: { type: String, required: false }, 
    uuid: { type: String, required: false },
    team: { type: Schema.Types.ObjectId, ref: 'Team', required: false },
    rank: { type: String, required: false },
    profileType: { type: String, required: true },
});

export interface Profile extends mongoose.Document {
    _id: string;
    ownerId: GlobalUser;
    platformName: string;
    accountName: string;
    emailRegistered?: string;
    uuid?: string;
    team?: TeamDto;
    rank?: string;
    profileType: ProfileType;
}

export class ProfileDto {
    @ApiProperty({
        description: 'Nombre del perfil'
    })
    ownerId: GlobalUser;

    @ApiProperty({
        description: 'Nombre del perfil'
    })
    platformName: PlatformName;

    @ApiProperty({
        description: 'Nombre con el que se le conoce'
    })
    accountName: string;

    @ApiProperty({
        description: 'Email registrado para esa cuenta'
    })
    emailRegistered?: string;

    @ApiPropertyOptional({
        description: 'Identificador único interno'
    })
    uuid?: string;

    @ApiPropertyOptional({
        description: 'Equipo si es jugador',
        type: TeamDto
    })
    team?: Team;

    @ApiPropertyOptional({
        description: 'Rango (si aplica)'
    })
    rank?: string;

    @ApiProperty({
        description: 'Tipo de rol asociado al perfil: Si es jugador, organizador, sponsor'
    })
    profileType: ProfileType;
}

export enum ProfileType {
    PERSONAL = 'PERSONAL',
    PLAYER = 'JUGADOR',
    SPONSOR = 'PATROCINADOR',
    ORGANIZER = 'ORGANIZADOR',
}

export enum PlatformName {
    RIOT_GAMES = 'RIOT_GAMES',
    DISCORD = 'DISCORD'
}