import { Controller, Delete, Post, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiBody, ApiTags } from '@nestjs/swagger';
import { JwtAuthGuard } from '../auth/JwtAuthGuard';
import { Roles } from '../role/decorators/role.decorator';
import { RoleGuard } from '../role/guards/role.guard';
import { UserStatus } from '../sso/models';
import { Status } from '../status/decorators/status.decorator';
import { StatusGuard } from '../status/guards/status.guard';
import { ProfileDto } from './models/profile.model';

@ApiTags('profiles')
@ApiBearerAuth()
@Controller('profile')
export class ProfileController {

    @Post(':userId/appendProfile')
    @Roles('admin', 'user')
    @Status(UserStatus.VALIDATED)
    @ApiBody({
        type: ProfileDto
    })
    appendProfileToUser(){

    }

    @Delete(':userId/removeProfile/:profileId')
    @Roles('admin')
    @Status(UserStatus.VALIDATED)
    removeProfileFromUser(){

    }
}
