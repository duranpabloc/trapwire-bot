import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { AuthModule } from '../auth/auth.module';
import { ProfileSchema } from './models/profile.model';
import { ProfileController } from './profile.controller';
import { ProfileService } from './profile.service';

@Module({
  imports: [MongooseModule.forFeature([{name: 'Profile', schema: ProfileSchema}])],
  controllers: [ProfileController],
  providers: [ProfileService]
})
export class ProfileModule {}
