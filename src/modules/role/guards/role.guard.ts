import { CanActivate, ExecutionContext, Injectable, Logger } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { AuthService } from 'src/modules/auth/auth.service';
import { SsoUserDto } from 'src/modules/sso/models';

@Injectable()
export class RoleGuard implements CanActivate {

  private readonly logger = new Logger(RoleGuard.name);

  constructor(private readonly _reflector: Reflector, private readonly _authService: AuthService) { }

  canActivate(
    context: ExecutionContext,
  ): boolean {
    const roles: string[] = this._reflector.get<string[]>('roles', context.getHandler(),);

    if (!roles) {
      return false;
    }

    const request = context.switchToHttp().getRequest();



    const { headers } = request;

    const bearerToken: string = headers.authorization;

    let payload: SsoUserDto;
    if(bearerToken){
      payload = this._authService.decodeJwt(bearerToken.split(' ')[1])
    }

    const hasRole = () => payload.roles.some((role: string) => { 
      return roles.includes(role);
    });

    return payload && payload.roles && hasRole();
  }
}
