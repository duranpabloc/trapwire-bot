import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import * as mongoose from 'mongoose';
import { Schema } from 'mongoose';

export const GlobalUserSchema = new mongoose.Schema({
    firstname: { type: String, required: true },
    lastname: { type: String, required: true },
    email: { type: String, required: true },
    address: { type: String, required: true },
    profiles: { type: [Schema.Types.ObjectId], ref: 'Profile', required: false },
    documentId: { type: String, required: true },    
    createdAt: { type: String, required: true },
    modifiedAt: { type: String, required: false }
});

export interface GlobalUser extends mongoose.Document {
    _id: string;
    ssoUser?: string;
    firstname: string;
    lastname: string;
    email: string;
    address: string;
    profiles?: string[];
    documentId: string;
    createdAt: string;
    modifiedAt?: string;
}

export class GlobalUserDto {
    @ApiProperty()
    firstname: string;

    @ApiProperty()
    lastname: string;

    @ApiProperty()
    email: string;

    @ApiProperty()
    address: string;

    @ApiPropertyOptional()
    profiles?: string[];

    @ApiProperty()
    documentId: string;

    @ApiPropertyOptional()
    createdAt: string;

    @ApiPropertyOptional()
    modifiedAt?: string;

    @ApiPropertyOptional()
    ssoUser?: string;
}