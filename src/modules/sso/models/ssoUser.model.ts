import { ApiPropertyOptional } from '@nestjs/swagger';
import * as mongoose from 'mongoose';
import { Schema } from 'mongoose';

export const SsoUserSchema = new mongoose.Schema({
    username: { type: String, required: true },
    password: { type: String, required: true },
    email: { type: String, required: true },
    roles: {type: [String], required: true},
    status: { type: String, required: true },
    globalUserId: { type: Schema.Types.ObjectId, ref: 'GlobalUser', required: false },
    lastLoggedIn: { type: String, required: false },
});

export interface SsoUser extends mongoose.Document {
    _id: string;
    username?: string;
    password?: string;
    email?: string;
    roles?: string[],
    status?: UserStatus;
    globalUserId?: string;
    lastLoggedIn?: string;
    jwt?: string;
}

export class SsoUserDto {
    @ApiPropertyOptional()
    username?: string;

    @ApiPropertyOptional()
    password?: string;

    @ApiPropertyOptional()
    email?: string;

    @ApiPropertyOptional()
    roles?: string[];

    @ApiPropertyOptional()
    status?: UserStatus;

    @ApiPropertyOptional()
    globalUserId?: string;

    @ApiPropertyOptional()
    lastLoggedIn?: string;

    @ApiPropertyOptional()
    jwt?: string;
}

export enum UserStatus {
    VALIDATING = 'VALIDANDO',
    VALIDATED = 'VALIDADO',
    INACTIVE = 'INACTIVO',
    BANNED = 'BANEADO'
}