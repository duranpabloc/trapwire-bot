import { Body, Controller, Get, HttpCode, HttpException, HttpStatus, Logger, Param, Post, UseGuards } from '@nestjs/common';
import { ApiOkResponse, ApiNotFoundResponse, ApiBody, ApiTags, ApiBadRequestResponse, ApiConflictResponse, ApiUnauthorizedResponse, ApiOperation, ApiBearerAuth, ApiParam } from '@nestjs/swagger';
import { AuthService } from 'src/modules/auth/auth.service';
import { JwtAuthGuard } from '../auth/JwtAuthGuard';
import { Roles } from '../role/decorators/role.decorator';
import { RoleGuard } from '../role/guards/role.guard';
import { Status } from '../status/decorators/status.decorator';
import { StatusGuard } from '../status/guards/status.guard';
import { GlobalUser, GlobalUserDto, SsoUserDto, UserStatus } from './models';
import { SsoService } from './sso.service';

@ApiTags('sso')
@Controller('sso')
export class SsoController {

    private readonly logger = new Logger(SsoController.name);

    constructor(
        private readonly ssoService: SsoService,
        private readonly authService: AuthService
    ) { }

    @Post('login')
    @HttpCode(200)
    @ApiOkResponse({
        description: 'La información se obtuvo correctamente'
    })
    @ApiNotFoundResponse({
        status: 404,
        description: 'No se encontró resultados para esa petición',
    })
    @ApiBody({ type: SsoUserDto })
    async loginUser(@Body() ssoUserDto: SsoUserDto): Promise<any> {
        const { username, password } = ssoUserDto;

        if (!username) {
            throw new HttpException(
                'Falta identificador de cuenta',
                HttpStatus.BAD_REQUEST,
            );
        }
        if (!password) {
            throw new HttpException(
                'No indicó contraseña',
                HttpStatus.BAD_REQUEST,
            );
        }
        try {
            const response = await this.ssoService.userExists(ssoUserDto);
            this.logger.debug(response);
            if (!response) {
                throw new HttpException(
                    'El usuario no existe',
                    HttpStatus.CONFLICT,
                );
            }
        } catch (error) {
            this.logger.error('Error loggear usuario',error.message, 'response');
            throw error;
        }
        try {
            let user = await this.ssoService.getUserByUserPassword(ssoUserDto);

            this.logger.debug(user, 'user');

            const payload = {
                ...user
            }

            ssoUserDto.jwt = await this.authService.signPayload(payload);

            return ssoUserDto.jwt;
        } catch (error) {
            this.logger.error('Usuario no encontrado', error.message, 'getUser')
            throw error;
        }

    }

    @Post('register-sso')
    @HttpCode(201)
    @ApiConflictResponse({
        description: 'El usuario ya existe'
    })
    @ApiUnauthorizedResponse({
        description: 'Acceso no autorizado'
    })
    @ApiBadRequestResponse({
        description: 'Falta token o company'
    })
    @ApiBody({ type: SsoUserDto })
    async createUser(@Body() userDto: SsoUserDto) {
        
        try {
            const response = await this.ssoService.userExists(userDto);
            this.logger.debug(response);
            if (response) {
                throw new HttpException(
                    'El usuario ya existe',
                    HttpStatus.CONFLICT,
                );
            }
        } catch (error) {
            this.logger.error('Error al crear usuario',error.message, 'response');
            throw error;
        }
        try {
            return await this.ssoService.registerSsoUser(userDto); 
        } catch (error) {
            this.logger.error('Error al crear usuario', error.message, 'createUser');
            throw error;
        }

    }

    @Post('register-user')
    @HttpCode(201)
    @ApiConflictResponse({
        description: 'El usuario ya existe'
    })
    @ApiUnauthorizedResponse({
        description: 'Acceso no autorizado'
    })
    @ApiBadRequestResponse({
        description: 'Falta token o company'
    })
    @ApiBody({ type: GlobalUserDto })
    @UseGuards(JwtAuthGuard)
    async createGlobalUser(@Body() globalUserDto: GlobalUserDto) {

        try {
            const globalUser = await this.ssoService.registerGlobalUser(globalUserDto); 

            this.ssoService.asociateGlobalUserToSsoUser(globalUser, globalUserDto.ssoUser)
            return 
        } catch (error) {
            this.logger.error('Error al crear usuario', error.message, 'createUser')
            throw error;
        }

    }

    @Get('users')
    @HttpCode(200)
    @ApiBearerAuth()
    @ApiOperation({ summary: 'Obtiene todos los usuarios' })
    @Roles('admin', 'user')
    @Status(UserStatus.VALIDATED)
    @UseGuards(JwtAuthGuard, RoleGuard, StatusGuard)
    async getAllUsers(): Promise<SsoUserDto[]> {
      try {
        const users = await this.ssoService.getAllUsers();
        return users;
      }
      catch (error) {
        this.logger.error(error);
      }
    }

    @Get('users/:ssoId')
    @HttpCode(200)
    @ApiBearerAuth()
    @Roles('admin', 'user')
    @Status(UserStatus.VALIDATED)
    @UseGuards(JwtAuthGuard, RoleGuard, StatusGuard)
    @ApiParam({ name: 'ssoId', description: 'ssoId', required: true, })
    @ApiOperation({ summary: 'Obtiene un GlobalUser asociado al ssoId' })
    async getGlobalUserBySsoId(@Param('ssoId') ssoId: string): Promise<GlobalUserDto> {
      try {
        const users = await this.ssoService.getGlobalUserBySsoId(ssoId);
        return users;
      }
      catch (error) {
        this.logger.error(error);
      }
    }
}
