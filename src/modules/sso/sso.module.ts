import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { AuthService } from 'src/modules/auth/auth.service';
import { ProfileModule } from 'src/modules/profile/profile.module';
import { GlobalUserSchema, SsoUserSchema } from './models';
import { SsoController } from './sso.controller';
import { SsoService } from './sso.service';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: 'GlobalUser', schema: GlobalUserSchema }]),
    MongooseModule.forFeature([{ name: 'SsoUser', schema: SsoUserSchema }]),
    ProfileModule
  ],
  controllers: [SsoController],
  providers: [SsoService, AuthService],
  exports: [SsoService]
})
export class SsoModule { }
