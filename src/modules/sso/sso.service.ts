import { Injectable, Logger } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import * as moment from 'moment';
import { Model } from 'mongoose';
import { GlobalUser, GlobalUserDto, SsoUser, SsoUserDto, UserStatus } from './models';

@Injectable()
export class SsoService {

    private readonly logger = new Logger(SsoService.name);

    constructor(
        @InjectModel('GlobalUser') private readonly globalUserModel: Model<GlobalUser>,
        @InjectModel('SsoUser') private readonly ssoUserModel: Model<SsoUser>
    ) { }

    async getSsoUserById(ssoId: string): Promise<SsoUser> {
        try {
            const user = await this.ssoUserModel.findOne({ _id: ssoId }).exec();

            return user as SsoUser;
        } catch (error) {
            this.logger.error(error.message);
        }
    }

    async getGlobalUserBySsoId(ssoId: string): Promise<GlobalUserDto> {
        try {
            this.logger.debug(ssoId);
            const ssoUser = await this.getSsoUserById(ssoId);
            const user = await this.globalUserModel.findOne({ _id: ssoUser.globalUserId }).exec();
            this.logger.debug(user);
            return user as GlobalUserDto;
        } catch (error) {
            this.logger.error(error.message);
        }
    }

    async getUserByUserPassword(ssoUserDto: SsoUserDto): Promise<GlobalUser> {
        try {
            const user = await this.ssoUserModel.findOne({ username: ssoUserDto.username, password: ssoUserDto.password }).exec();
            user.lastLoggedIn = moment().format('YYYY-MM-DD HH:mm');

            user.save();
            return user as GlobalUser;
        } catch (error) {
            this.logger.error(error.message);
        }
    }

    async getAllUsers(): Promise<SsoUserDto[]> {
        try {
            const users = await this.ssoUserModel.find({});
            return users as SsoUserDto[];
        } catch (error) {
            this.logger.error(error);
        }
    }

    async userExists(ssoUserDto: SsoUserDto): Promise<SsoUser> {
        try {
            const user = await this.ssoUserModel.findOne({ username: ssoUserDto.username }).exec();
            return user as SsoUser;
        } catch (error) {
            this.logger.error(error);
        }
    }

    async registerSsoUser(ssoUser: SsoUserDto): Promise<SsoUser> {
        try {
            const { username, password, email } = ssoUser;
            const userModel = new this.ssoUserModel({
                username: username,
                password: password,
                email: email,
                roles: ['user'],
                status: UserStatus.VALIDATING
            });

            return await userModel.save();

        } catch (error) {
            this.logger.error(error);
            throw new Error(`Can't create GlobalUser: ${error.message}`);
        }
    }

    async asociateGlobalUserToSsoUser(globalUser: GlobalUser, ssoUserId: string): Promise<SsoUser> {
        try {
            const user = await this.getSsoUserById(ssoUserId);

            await user.updateOne({ globalUserId: globalUser._id});

            return user.save();

        } catch (error) {
            this.logger.error(error);
            throw new Error(`Can't create GlobalUser: ${error.message}`);
        }
    }

    async registerGlobalUser(globalUser: GlobalUserDto): Promise<GlobalUser> {
        try {
            const { address, documentId, firstname, lastname, email } = globalUser;
            const userModel = new this.globalUserModel({
                firstname: firstname,
                lastname: lastname,
                email: email,
                documentId: documentId,
                address: address,
                createdAt: moment().format('YYYY-MM-DD HH:mm'),
            });

            return await userModel.save();

        } catch (error) {
            this.logger.error(error);
            throw new Error(`Can't create GlobalUser: ${error.message}`);
        }
    }
}
