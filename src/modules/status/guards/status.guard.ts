import { CanActivate, ExecutionContext, Injectable, Logger } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { Observable } from 'rxjs';
import { AuthService } from 'src/modules/auth/auth.service';
import { SsoUserDto } from 'src/modules/sso/models';

@Injectable()
export class StatusGuard implements CanActivate {
  
  private readonly logger = new Logger(StatusGuard.name);

  constructor(private readonly _reflector: Reflector, private readonly _authService: AuthService) { }

  canActivate(
    context: ExecutionContext,
  ): boolean {
    const status: string = this._reflector.get<string>('status', context.getHandler(),);

    if (!status) {
      return false;
    }

    const request = context.switchToHttp().getRequest();



    const { headers } = request;

    const bearerToken: string = headers.authorization;

    let payload: SsoUserDto;
    if(bearerToken){
      payload = this._authService.decodeJwt(bearerToken.split(' ')[1])
    }
    
    const hasStatus = () => payload.status == status;
    

    return payload && payload.status && hasStatus();
  }
}
