import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import * as mongoose from 'mongoose';
import { GlobalUser } from 'src/modules/sso/models';

export const TeamSchema = new mongoose.Schema({
    name: { type: String, required: true },
    members: { type: Array, required: false },
    captain: { type: Object, required: false },
    contacts: { type: Array, required: false },
    sponsors: { type: [String], required: false },
    active: { type: Boolean, required: false },
    professional: { type: Boolean, required: false },
    website: { type: String, required: false },
    facebook: { type: String, required: false },
    twitter: { type: String, required: false },
    twitch: { type: String, required: false },
});

export interface Team extends mongoose.Document {
    _id: string;
    name: string;
    members?: GlobalUser[];
    captain?: GlobalUser;
    contacts?:GlobalUser[];
    sponsors?:string[];
    active?: boolean;
    professional?: boolean;
    website?: string;
    facebook: string;
    twitter: string;
    twitch:  string;
}

export class TeamDto{

    @ApiPropertyOptional()
    id?:string;

    @ApiProperty()
    name: string;

    @ApiPropertyOptional()
    members?: GlobalUser[];

    @ApiPropertyOptional()
    captain?: GlobalUser;

    @ApiPropertyOptional()
    contacts?:GlobalUser[];

    @ApiPropertyOptional()
    sponsors?:string[];

    @ApiPropertyOptional()
    active?: boolean;

    @ApiPropertyOptional()
    professional?: boolean;

    @ApiPropertyOptional()
    website?: string;

    @ApiPropertyOptional()
    facebook?: string;

    @ApiPropertyOptional()
    twitter?: string;

    @ApiPropertyOptional()
    twitch?:  string;
}