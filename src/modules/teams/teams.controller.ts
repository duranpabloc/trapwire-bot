import { Body, Controller, HttpCode, HttpException, HttpStatus, Logger, Post, UseGuards } from '@nestjs/common';
import { ApiBody, ApiTags, ApiNoContentResponse, ApiBadRequestResponse, ApiConflictResponse, ApiUnauthorizedResponse, ApiBearerAuth } from '@nestjs/swagger';
import { JwtAuthGuard } from '../auth/JwtAuthGuard';
import { Roles } from '../role/decorators/role.decorator';
import { RoleGuard } from '../role/guards/role.guard';
import { UserStatus } from '../sso/models';
import { Status } from '../status/decorators/status.decorator';
import { StatusGuard } from '../status/guards/status.guard';
import { TeamDto } from './team.model';
import { TeamsService } from './teams.service';

@ApiTags('teams')
@ApiBearerAuth()
@Controller('teams')
export class TeamsController {
    private readonly logger = new Logger(TeamsController.name);
    
    constructor(private readonly teamsService: TeamsService){}

    @Post('register')
    @HttpCode(204)
    @ApiNoContentResponse({
        status: 204,
        description: 'El usuario se creó correctamente.',
    })
    @ApiConflictResponse({
        description: 'El usuario ya existe'
    })
    @ApiUnauthorizedResponse({
        description: 'Acceso no autorizado'
    })
    @ApiBadRequestResponse({
        description: 'Falta token'
    })
    @ApiBody({ type: TeamDto })
    @Roles('admin', 'user')
    @Status(UserStatus.VALIDATED)
    @UseGuards(JwtAuthGuard, RoleGuard, StatusGuard)
    async registerTeam(@Body() teamDto: TeamDto) {
        
        try {
            const response = await this.teamsService.getTeamByName(teamDto);
            if (response.name) {
                throw new HttpException(
                    'El team ya existe',
                    HttpStatus.CONFLICT,
                );
            }
        } catch (error) {
            throw error;
        }
        try {
            return await this.teamsService.registerTeam(teamDto); 
        } catch (error) {
            this.logger.error('Error al ejecutar la solicitud', error.message, 'registerTeam')
            throw error;
        }

    }
}
