import { Injectable, Logger } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Team, TeamDto } from './team.model';

@Injectable()
export class TeamsService {

    private readonly logger = new Logger(TeamsService.name);

    constructor(@InjectModel('Team') private readonly teamModel: Model<Team>){}

    async getTeamById(teamDto: TeamDto): Promise<Team> {
        try {
            const user = await this.teamModel.findOne({ _id: teamDto.id}).exec();
            return user as Team;
        } catch (error) {
            this.logger.error(error);
        }
    }

    async getTeamByName(teamDto: TeamDto): Promise<Team> {
        try {
            const user = await this.teamModel.findOne({ name: teamDto.name}).exec();
            return user as Team;
        } catch (error) {
            this.logger.error(error);
        }
    }

    

    async registerTeam(team: TeamDto): Promise<TeamDto> {
        try {
            const { name, captain } = team;
            const newTournament = new this.teamModel({
                name: name,
                active: true,
                captain: captain,
                professional: false
            });
            return await newTournament.save();
        } catch (error) {
            this.logger.error(error);
            throw new Error(`Can't create Team: ${error.message}`);
        }
    }
}
