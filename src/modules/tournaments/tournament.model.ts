import * as mongoose from 'mongoose';
import { Team } from 'src/modules/teams/team.model';
import { ApiProperty, ApiHideProperty } from '@nestjs/swagger';
import { GlobalUser } from 'src/modules/sso/models';


export const TournamentSchema = new mongoose.Schema({
    name: { type: String, required: true },
    startDate: { type: Date, required: true },
    organizerId: { type: Number, required: true },
    external: { type: Boolean, required: true },
    participants: { type: Array, required: false },
    teams: { type: Array, required: false },
    status: { type: String, required: true }
});

export interface Tournament extends mongoose.Document {
    _id: string;
    name: string;
    startDate: Date;
    organizerId: number;
    external: boolean;
    participants?: GlobalUser[];
    teams?: Team[];
    status: TournamentStatus;
}

export class TournamentDto {
    @ApiProperty({example: 'Team Pepito'})
    name: string;

    @ApiProperty({ example: '2020-07-26T01:00:00.000-04:00'})
    startDate: Date;

    @ApiProperty({example: 10})
    organizerId: number;

    @ApiProperty({ example: false})
    external: boolean;

    @ApiProperty({ example: []})
    participants?: GlobalUser[];

    @ApiProperty({example: []})
    teams?: Team[];

    @ApiHideProperty()
    status: TournamentStatus;
}

export enum TournamentStatus {
    IDLE = 'IDLE',
    REGISTRATION = 'REGISTRATION',
    CHECK_IN = 'CHECK IN',
    ON_GOING = 'ON GOING',
    FINISHED = 'FINISHED'
}