import { Body, Controller, Get, HttpCode, Logger, NotFoundException, Post, UseGuards } from '@nestjs/common';
import { ApiTags, ApiOperation, ApiBody, ApiCreatedResponse, ApiNoContentResponse, ApiBearerAuth } from '@nestjs/swagger';
import { JwtAuthGuard } from '../auth/JwtAuthGuard';
import { Roles } from '../role/decorators/role.decorator';
import { RoleGuard } from '../role/guards/role.guard';
import { UserStatus } from '../sso/models';
import { Status } from '../status/decorators/status.decorator';
import { StatusGuard } from '../status/guards/status.guard';
import { TournamentDto } from './tournament.model';
import { TournamentsService } from './tournaments.service';

@ApiTags('tournaments')
@ApiBearerAuth()
@Controller('tournaments')
export class TournamentsController {

  private readonly logger = new Logger(TournamentsController.name);

  constructor(private readonly tournamentsService: TournamentsService) { }


  @Get('next')
  @HttpCode(200)
  @ApiOperation({ summary: 'Obtiene los siguientes torneos' })
  @Roles('admin', 'user')
  @Status(UserStatus.VALIDATED)
  @UseGuards(JwtAuthGuard, RoleGuard, StatusGuard)
  async getNextTournaments(): Promise<TournamentDto[]> {
    try {
      return await this.tournamentsService.getNextTournaments();
    }
    catch (error) {
      this.logger.error(error);
    }
  }


  @Get('onRegistration')
  @HttpCode(200)
  @ApiOperation({ summary: 'Obtiene torneo que está en estado de registro' })
  @Roles('admin', 'user')
  @Status(UserStatus.VALIDATED)
  @UseGuards(JwtAuthGuard, RoleGuard, StatusGuard)
  async getActiveTournament(): Promise<TournamentDto> {
    try {
      return await this.tournamentsService.getOnRegistrationTournament();
    }
    catch (error) {
      this.logger.error(error);
    }
  }


  @Get('onCheckIn')
  @HttpCode(200)
  @ApiOperation({ summary: 'Obtiene torneo que está en estado de check In' })
  @Roles('admin', 'user')
  @Status(UserStatus.VALIDATED)
  @UseGuards(JwtAuthGuard, RoleGuard, StatusGuard)
  async getOnCheckInTournament(): Promise<TournamentDto> {
    try {
      const result = await this.tournamentsService.getOnCheckInTournament();
      this.logger.debug(result);
      return result;
    }
    catch (error) {
      this.logger.error(error);
      throw new NotFoundException('No hay torneos que estén haciendo check in');
    }
  }


  @Get('onGoing')
  @HttpCode(200)
  @ApiOperation({ summary: 'Obtiene torneo que ya finalizó check in' })
  @Roles('admin', 'user')
  @Status(UserStatus.VALIDATED)
  @UseGuards(JwtAuthGuard, RoleGuard, StatusGuard)
  async getOnGoingTournament(): Promise<TournamentDto> {
    try {
      return await this.tournamentsService.getOnGoingTournament();
    }
    catch (error) {
      this.logger.error(error);
      throw new NotFoundException('No hay torneos que se estén jugando en estos momentos');
    }
  }

  @Post('add')
  @ApiOperation({ summary: 'Crear torneo' })
  @ApiBody({ type: TournamentDto })
  @HttpCode(201)
  @ApiCreatedResponse({
    description: 'Torneo creado',
    type: TournamentDto,
  })
  @Roles('admin', 'user')
  @Status(UserStatus.VALIDATED)
  @UseGuards(JwtAuthGuard, RoleGuard, StatusGuard)
  async createTournament(@Body() request: TournamentDto): Promise<TournamentDto> {
    try {
      this.logger.debug(request);
      const response = await this.tournamentsService.createTournament(request);
      this.logger.debug(response);

      return response;
    }
    catch (error) {
      this.logger.error(error);
    }
  }

  @Post('startRegistration')
  @ApiOperation({ summary: 'Comienza el registro para torneo en espera' })
  @HttpCode(204)
  @ApiNoContentResponse()
  @Roles('admin', 'user')
  @Status(UserStatus.VALIDATED)
  @UseGuards(JwtAuthGuard, RoleGuard, StatusGuard)
  async startRegistration(): Promise<void> {
    try {
      await this.tournamentsService.startRegistration((await this.tournamentsService.getOnTimeTournament())._id);
    }
    catch (error) {
      this.logger.error(error);
    }
  }

  @Post('endRegistration')
  @ApiOperation({ summary: 'Finaliza registro de torneo en curso' })
  @HttpCode(204)
  @ApiNoContentResponse()
  @Roles('admin', 'user')
  @Status(UserStatus.VALIDATED)
  @UseGuards(JwtAuthGuard, RoleGuard, StatusGuard)
  async endRegistration(): Promise<void> {
    try {
      await this.tournamentsService.endRegistration((await this.tournamentsService.getOnRegistrationTournament())._id);
    }
    catch (error) {
      this.logger.error(error);
    }
  }

  @Post('startTournament')
  @ApiOperation({ summary: 'Comienza el torneo en curso' })
  @HttpCode(204)
  @ApiNoContentResponse()
  @Roles('admin', 'user')
  @Status(UserStatus.VALIDATED)
  @UseGuards(JwtAuthGuard, RoleGuard, StatusGuard)
  async startTournament(): Promise<void> {
    try {
      await this.tournamentsService.endCheckIn((await this.tournamentsService.getOnCheckInTournament())._id);
    }
    catch (error) {
      this.logger.error(error);
    }
  }

  @Post('finishTournament')
  @ApiOperation({ summary: 'Finaliza torneo en curso' })
  @HttpCode(204)
  @ApiNoContentResponse()
  @Roles('admin', 'user')
  @Status(UserStatus.VALIDATED)
  @UseGuards(JwtAuthGuard, RoleGuard, StatusGuard)
  async finishTournament(): Promise<void> {
    try {
      await this.tournamentsService.finishTournament((await this.tournamentsService.getOnGoingTournament())._id);
    }
    catch (error) {
      this.logger.error(error);
    }
  }
}
