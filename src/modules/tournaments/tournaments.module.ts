import { Module } from '@nestjs/common';
import { TournamentsController } from './tournaments.controller';
import { TournamentsService } from './tournaments.service';
import { MongooseModule } from '@nestjs/mongoose';
import { TournamentSchema } from './tournament.model';
import { AuthModule } from '../auth/auth.module';

@Module({
  imports: [MongooseModule.forFeature([{name: 'Tournament', schema: TournamentSchema}]), AuthModule],
  controllers: [TournamentsController],
  providers: [TournamentsService],
  exports: [TournamentsService]
})
export class TournamentsModule {}
