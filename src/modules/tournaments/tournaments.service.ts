import { Injectable, Logger } from '@nestjs/common';
import { TournamentDto, Tournament, TournamentStatus } from './tournament.model';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import * as moment from 'moment-timezone';

@Injectable()
export class TournamentsService {
    constructor(@InjectModel('Tournament') private readonly tournamentModel: Model<Tournament>) { }

    private readonly logger = new Logger(TournamentsService.name);

    async createTournament(tournament: TournamentDto): Promise<TournamentDto> {
        try {
            const newTournament = new this.tournamentModel({ name: tournament.name, organizerId: tournament.organizerId, participants: tournament.participants, startDate: tournament.startDate , status: TournamentStatus.IDLE, external: false });
            return await newTournament.save();
        } catch (error) {
            this.logger.error(error);
            throw new Error(`Can't create Tournament: ${error.message}`);
        }
    }

    async getNextTournaments(): Promise<Tournament[]> {
        try {
            const tournaments = await this.tournamentModel.find({ status: TournamentStatus.IDLE, startDate: { $gt: new Date(moment.tz("America/Santiago").format()) }}).exec();
            return tournaments as Tournament[];
        } catch (error) {
            this.logger.error(error);
        }
    }

    async getOnTimeTournament(): Promise<Tournament> {
        try {
            const tournament = await this.tournamentModel.findOne({ startDate: { $lte: new Date(moment().format()) }, status: TournamentStatus.IDLE }).exec();
            return tournament as Tournament;
        } catch (error) {
            this.logger.error(error);
        }
    }

    async getOnRegistrationTournament(): Promise<Tournament> {
        try {
            const tournament = await this.tournamentModel.findOne({ status: TournamentStatus.REGISTRATION }).exec();
            return tournament as Tournament;
        } catch (error) {
            this.logger.error(error);
        }
    }

    async getOnCheckInTournament(): Promise<Tournament> {
        try {
            const tournament = await this.tournamentModel.findOne({ status: TournamentStatus.CHECK_IN }).exec();

            return tournament as Tournament;
        } catch (error) {
            this.logger.error(error);
        }
    }

    async getOnGoingTournament(): Promise<Tournament> {
        try {
            const tournament = await this.tournamentModel.findOne({ startDate: { $lte: new Date(moment().format()) }, status: TournamentStatus.ON_GOING }).exec();
            return tournament as Tournament;
        } catch (error) {
            this.logger.error(error);
        }
    }
    /**
     * Comienza la etapa de registro del torneo indicado.
     * @param id Id del torneo en la base de datos
     */
    async startRegistration(id: string): Promise<void> {
        try {
            this.logger.debug(moment().format());
        } catch (error) {
            this.logger.error(error);
        }
    }
    /**
     * Termina el periodo de registro del torneo indicado
     * @param id Id del torneo en la base de datos
     */
    async endRegistration(id: string): Promise<void> {
        try {
            this.logger.debug(moment().format());
        } catch (error) {
            this.logger.error(error);
        }
    }
    /**
     * Termina la etapa de check in y comienza oficialmente el torneo
     * @param id Id del torneo en la base de datos
     */
    async endCheckIn(id: string): Promise<void> {
        try {
            this.logger.debug(moment().format());
        } catch (error) {
            this.logger.error(error);
        }
    }
    /**
     * Finaliza el torneo indicado con el id indicado que esté en estado ON GOING
     * @param id Id del torneo en la base de datos
     */
    async finishTournament(id: string): Promise<void> {
        try {
            this.logger.debug(moment().format());
        } catch (error) {
            this.logger.error(error);
        }
    }
}
